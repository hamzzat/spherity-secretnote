import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { CreateNoteDto } from 'src/note/dto/create-note.dto';
import { decrypt } from '../src/core/utils';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let dto: CreateNoteDto;
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    dto = {
      note: 'lorem ipsum dolor sit amet',
    };
    await request(app.getHttpServer()).post('/api/v1/note').send(dto);
  });

  describe('Create New Secret Note  POST /api/v1/note', () => {
    it(' create a new note', () => {
      return request(app.getHttpServer())
        .post('/api/v1/note')
        .send(dto)
        .expect(201);
    });

    // it('should validate a note and give error', () => {
    //   const dto: CreateNoteDto = {
    //     note: '""',
    //   };
    //   return request(app.getHttpServer())
    //     .post('/api/v1/note')
    //     .send(dto)
    //     .expect(400).expect((data)=>{
    //       expect(data.body.message).toBe("Value must not be empty")
    //     })
    // });
  });

  describe('Find  Secret Note  GET /api/v1/note', () => {
    it('find all secret note', async () => {
      return request(app.getHttpServer())
        .get('/api/v1/note')
        .expect(200)
        .expect((data) => {
          expect(data.body.length).toBeGreaterThan(0);
        });
    });

    it('find all decreypted secret note', async () => {
      return request(app.getHttpServer())
        .get('/api/v1/note/?decrypted=true')
        .expect(200)
        .expect((data) => {
          expect(data.body.length).toBeGreaterThan(0);
          data.body.forEach((note) => {
            expect(note.note).toBe(dto.note);
          });
        });
    });

    it('should throw error if note id not found', async () => {
      return request(app.getHttpServer())
        .get(`/api/v1/note/${10}`)
        .expect(404)
        .expect((data) => {
          expect(data.body.message).toBe('Note #10 not found');
        });
    });

    it('find one secret note', async () => {
      const dat = await request(app.getHttpServer())
        .post('/api/v1/note')
        .send(dto);
      return request(app.getHttpServer())
        .get(`/api/v1/note/${dat.body.id}`)
        .expect(200)
        .expect((data) => {
          expect(data.body.note).toBe(dat.body.note);
        });
    });
  });

  describe(' Update and Delete /api/v1/note', () => {
    it('update a note', async () => {
      const note_data = {
        note: 'lorem ipsum dolor',
      };
      const note = await request(app.getHttpServer())
        .post('/api/v1/note')
        .send(dto);
      return request(app.getHttpServer())
        .patch(`/api/v1/note/${note.body.id}`)
        .send(note_data)
        .expect(200)
        .expect((data) => {
          expect(decrypt(data.body.note)).toBe(note_data.note);
        });

        
    });

    it('should throw error if note id not found for PATCH', async () => {
      return request(app.getHttpServer())
        .patch(`/api/v1/note/${100}`)
        .expect(404)
        .expect((data) => {
          expect(data.body.message).toBe('Note #100 not found');
        });
    });

    it('Delete a note', async () => {
      const note = await request(app.getHttpServer())
        .post('/api/v1/note')
        .send(dto);
      return request(app.getHttpServer())
        .delete(`/api/v1/note/${note.body.id}`)
        .expect(200)
        .expect((data) => {
          expect(decrypt(data.body.note)).toBe(dto.note);
        });
    });

    it('should throw error if note id not found For DELETE', async () => {
      return request(app.getHttpServer())
        .delete(`/api/v1/note/${100}`)
        .expect(404)
        .expect((data) => {
          expect(data.body.message).toBe('Note #100 not found');
        });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
