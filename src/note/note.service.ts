import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { PrismaService } from '../core/prisma/prisma.service';
import { encrypt, decrypt } from '../core/utils';
import { secretNote } from '@prisma/client';
@Injectable()
export class NoteService {
  constructor(private prisma: PrismaService) {}

  public async create(createNoteDto: CreateNoteDto): Promise<secretNote> {
    const encryptedNote = encrypt(createNoteDto.note);
    if(encryptedNote){
    
    return await this.prisma.secretNote.create({
      data: {
        note: encryptedNote,
      },
    });
  }
  }

  public async findAll(decrypted?: boolean): Promise<secretNote[]> {
    try{
    const noteQuery: secretNote[] = await this.prisma.secretNote.findMany();
    if (!decrypted) {
      return noteQuery;
    } else {
      return noteQuery.map((note) => {
        note.note = decrypt(note.note);
        return note;
      });
    }}catch (e) {
      throw e;
    }
  }

  public async findOne(id: number, decrypted?: boolean): Promise<secretNote> {
    const data: secretNote = await this.prisma.secretNote.findFirst({ where: { id } });
    if (!data) {
      throw new NotFoundException(`Note #${id} not found`);
    }
    if (!decrypted) {
      return data;
    } else {
      data.note = decrypt(data.note);
      return data;
    }

  }



  public async update(
    id: number,
    updateNoteDto: UpdateNoteDto,
  ): Promise<secretNote> {

    
     const note:secretNote = await this.prisma.secretNote.findFirst({ where: { id: id }});
     try{
     if(!note) {
      throw new NotFoundException(`Note #${id} not found`);

     }else{
    const encryptedNote = encrypt(updateNoteDto.note);
    return await this.prisma.secretNote.update({
      where: { id },
      data: { note: encryptedNote },
    });

  }
}
  catch (e) {
    throw e;
  }
  }

  public async remove(id: number): Promise<secretNote> {
    try{
    const note = await this.prisma.secretNote.findFirst({ where: { id: id }});
    if(!note) {
      throw new NotFoundException(`Note #${id} not found`);
  }
   const deleted_note: secretNote =await this.prisma.secretNote.delete({ where: { id } });

    return deleted_note;
  }catch (e) {
  throw e;
}
}
}
