import { Test } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import { CreateNoteDto } from 'src/note/dto/create-note.dto';
import { PrismaService } from 'src/core/prisma/prisma.service';
import { decrypt} from 'src/core/utils';
import { NoteService } from '../note.service';

describe('NoteeService', () => {
  let prisma: PrismaService;
  let noteService: NoteService;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    prisma = moduleRef.get(PrismaService);
    noteService = moduleRef.get<NoteService>(NoteService);
    await prisma.cleanDatabse();
  });

  describe('createTodo()', () => {
    const dto: CreateNoteDto = {
      note: 'lorem ipsum dolor sit amet',
    };

    it('should create note', async () => {
      const note = await noteService.create(dto);
      expect(decrypt(note.note)).toBe(dto.note);
    });
  });

  describe('Find Note', () => {
    it('should find note by id', async () => {
      const note = await noteService.findOne(1);
      expect(decrypt(note.note)).toBe('lorem ipsum dolor sit amet');
      expect(note.id).toBe(1);
    });
    
    it('should find note by id without decryption', async () => {
      const note = await noteService.findOne(1, true);      
      expect(note.note).toBe('lorem ipsum dolor sit amet');
      expect(note.id).toBe(1);
    });
    
    it('should find all note', async () => {
      const note = await noteService.findAll();
      expect(note.length).toBeGreaterThan(0);
    });
  });
  
  describe('Update Note', () => {
    const updateNoteDto = {
      note: 'lorem ipsum dolor',
    };

    it('should update note without', async () => {
      const note = await noteService.findOne(1);
      const updatedNote = await noteService.update(note.id, updateNoteDto);
      expect(decrypt(updatedNote.note)).toBe(updateNoteDto.note);
    });
  });

  describe('Delete Note()', () => {
    it('should delete note without', async () => {
      const note = await noteService.findOne(1);
      const deletedNote = await noteService.remove(note.id);
      expect(deletedNote.note).toBe(note.note);
    });
  });
});
